package main.java.com.tsc.apogasiy.tm.api;

public interface ICommandController {
    void showCommands();

    void showArguments();

    void showHelp();

    void showVersion();

    void showAbout();

    void showWelcome();

    void showError();

    void showInfo();

    void exit();
}
