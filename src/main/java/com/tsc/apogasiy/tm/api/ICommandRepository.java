package main.java.com.tsc.apogasiy.tm.api;

import main.java.com.tsc.apogasiy.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
