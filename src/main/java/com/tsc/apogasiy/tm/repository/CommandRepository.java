package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.ICommandRepository;
import main.java.com.tsc.apogasiy.tm.constant.ArgumentConst;
import main.java.com.tsc.apogasiy.tm.constant.TerminalConst;
import main.java.com.tsc.apogasiy.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command INFO = new Command(TerminalConst.INFO, ArgumentConst.INFO, "Display memory info.");
    public static final Command ABOUT = new Command(TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info.");
    public static final Command VERSION = new Command(TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version.");
    public static final Command HELP = new Command(TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands.");
    public static final Command EXIT = new Command(TerminalConst.EXIT, null, "Close application.");
    public static final Command COMMANDS = new Command(TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Display list commands.");
    public static final Command ARGUMENTS = new Command(TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display list arguments.");

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, HELP, VERSION, ARGUMENTS, COMMANDS, EXIT
    };

    public Command[] getCommands() {
        return TERMINAL_COMMANDS;
    }

}
