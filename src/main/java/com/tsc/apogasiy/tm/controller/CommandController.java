package main.java.com.tsc.apogasiy.tm.controller;

import main.java.com.tsc.apogasiy.tm.api.ICommandService;
import main.java.com.tsc.apogasiy.tm.model.Command;
import main.java.com.tsc.apogasiy.tm.util.NumberUtil;
import main.java.com.tsc.apogasiy.tm.api.ICommandController;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showCommands() {
        for (final Command command : commandService.getCommands()) {
            showCommandValue(command.getName());
        }
    }

    @Override
    public void showArguments() {
        for (final Command command : commandService.getCommands()) {
            showCommandValue(command.getArgument());
        }
    }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty())
            return;
        System.out.println(value);
    }

    public void showHelp() {
        for (final Command command : commandService.getCommands()) {
            System.out.println(command);
        }
    }

    public void showVersion() {
        System.out.println("1.0.0");
    }

    public void exit() {
        System.exit(0);
    }

    public void showAbout() {
        System.out.println("Alexey Pogasiy");
        System.out.println("apogasiy@tsconsulting.com");
    }

    public void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public void showError() {
        System.out.println("Unknown command! Type 'help' for supportable command!");
    }

    public void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long userMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(userMemory));
    }
}
