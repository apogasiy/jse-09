package main.java.com.tsc.apogasiy.tm.component;

import main.java.com.tsc.apogasiy.tm.api.ICommandController;
import main.java.com.tsc.apogasiy.tm.api.ICommandRepository;
import main.java.com.tsc.apogasiy.tm.api.ICommandService;
import main.java.com.tsc.apogasiy.tm.constant.ArgumentConst;
import main.java.com.tsc.apogasiy.tm.constant.TerminalConst;
import main.java.com.tsc.apogasiy.tm.controller.CommandController;
import main.java.com.tsc.apogasiy.tm.repository.CommandRepository;
import main.java.com.tsc.apogasiy.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private static final ICommandRepository commandRepository = new CommandRepository();
    private static final ICommandService commandService = new CommandService(commandRepository);
    private static final ICommandController commandController = new CommandController(commandService);

    public Bootstrap() {
    }

    public void start(final String[] args) {
        commandController.showWelcome();
        parseArgs(args);
        process();
    }

    public void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            parseCommand(command);
            System.out.println();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.showError();
                break;
        }
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showError();
                break;
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        for (String param : args)
            parseArg(param);
    }
}
